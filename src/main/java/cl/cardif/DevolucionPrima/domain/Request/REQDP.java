package cl.cardif.DevolucionPrima.domain.Request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class REQDP{
	@JsonProperty("DevolucionPrimaRequest")
	private DevolucionPrimaREQ devolucionPrimaREQ;
	
	public DevolucionPrimaREQ getDevolucionPrimaREQ() {
		return devolucionPrimaREQ;
	}
}
