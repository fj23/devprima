package cl.cardif.DevolucionPrima.domain.Request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DevolucionPrimaREQ {
	@JsonProperty("IdSocio")
	private String idSocio;
	@JsonProperty("RutAsegurado")
	private String rutAsegurado;
	@JsonProperty("OperacionSocio")
	private String operacionSocio;
	@JsonProperty("PolizaIndividual")
	private String polizaIndividual;
	@JsonProperty("MotivoCancelacion")
	private String motivoCancelacion;
	@JsonProperty("PolizaMadre")
	private String polizaMadre;
	@JsonProperty("FechaTermino")
	private String fechaTermino;
	@JsonProperty("NombreEjecutivo")
	private String nombreEjecutivo;
	@JsonProperty("Sucursal")
	private String sucursal;
	@JsonProperty("NombreCargo")
	private String nombreCargo;
	@JsonProperty("RutEjecutivo")
	private String rutEjecutivo;
	@JsonProperty("IdefJefeEjecutivo")
	private String idefJefeEjecutivo;
	public String getIdefJefeEjecutivo() {
		return idefJefeEjecutivo;
	}
	public String getIdSocio() {
		return idSocio;
	}
	public String getRutAsegurado() {
		return rutAsegurado;
	}
	public String getOperacionSocio() {
		return operacionSocio;
	}
	public String getPolizaIndividual() {
		return polizaIndividual;
	}
	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}
	public String getPolizaMadre() {
		return polizaMadre;
	}
	public String getFechaTermino() {
		return fechaTermino;
	}
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}
	public String getSucursal() {
		return sucursal;
	}
	public String getNombreCargo() {
		return nombreCargo;
	}
	public String getRutEjecutivo() {
		return rutEjecutivo;
	}


}
