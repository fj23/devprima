package cl.cardif.DevolucionPrima.domain.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DevolucionPrimaRSP {
	@JsonProperty("OperacionCardif")
	private String operacionCardif;
	@JsonProperty("PolizaMadre")
	private String polizaMadre;
	@JsonProperty("PermiteDevolver")
	private String permiteDevolver;
	@JsonProperty("MontoDevolucionUF")
	private String montoDevolucionUF;
	@JsonProperty("MontoDevolucionPesos")
	private String montoDevolucionPesos;
	@JsonProperty("FechaUfUtilizada")
	private String fechaUfUtilizada;
	@JsonProperty("ValorUfUtilizada")
	private String valorUfUtilizada;
	@JsonProperty("GlosaDescripcionError")
	private String glosaDescripcionError;
	public DevolucionPrimaRSP(String operacionCardif, String polizaMadre, String permiteDevolver, String montoDevolucionUF, String montoDevolucionPesos, String fechaUfUtilizada,
			String valorUfUtilizada,String glosaDescripcionError) {
		super();
		this.operacionCardif = operacionCardif;
		this.polizaMadre = polizaMadre;
		this.permiteDevolver = permiteDevolver;
		this.montoDevolucionUF = montoDevolucionUF;
		this.montoDevolucionPesos = montoDevolucionPesos;
		this.fechaUfUtilizada = fechaUfUtilizada;
		this.valorUfUtilizada = valorUfUtilizada;
		this.glosaDescripcionError = glosaDescripcionError;
	}

	

	
	
}
