package cl.cardif.DevolucionPrima.domain.Response;

public class MotivosRsp {

	private String codigoMotivo;
	private String motivo;
	public String getCodigoMotivo() {
		return codigoMotivo;
	}
	public String getMotivo() {
		return motivo;
	}
	public MotivosRsp(String codigoMotivo, String motivo) {
		super();
		this.codigoMotivo = codigoMotivo;
		this.motivo = motivo;
	}
	
	
}
