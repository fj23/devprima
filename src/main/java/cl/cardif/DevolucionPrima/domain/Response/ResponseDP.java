package cl.cardif.DevolucionPrima.domain.Response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDP {
	@JsonProperty("RutAsegurado")
	private String rutAsegurado;
	@JsonProperty("OperacionSocio")
	private String operacionSocio;
	@JsonProperty("PolizaIndividual")
	private String polizaIndividual;
	@JsonProperty("FechaTerminoAnticipado")
	private String fechaTerminoAnticipado;
	@JsonProperty("MotivoDevolucion")
	private String motivoDevolucion;
	@JsonProperty("DevolucionPrimaResponse")
	private List<DevolucionPrimaRSP> devolucionPrimaRSP;
	private List<TextsResponse> textsResponse;
	@JsonProperty("Message")
	private Message message;
	
	public ResponseDP(String rutAsegurado, String operacionSocio, String polizaIndividual,
			String fechaTerminoAnticipado, String motivoDevolucion, List<DevolucionPrimaRSP> devolucionPrimaRSP,List<TextsResponse> textsResponse,
			Message message) {
		super();
		this.rutAsegurado = rutAsegurado;
		this.operacionSocio = operacionSocio;
		this.polizaIndividual = polizaIndividual;
		this.fechaTerminoAnticipado = fechaTerminoAnticipado;
		this.motivoDevolucion = motivoDevolucion;
		this.devolucionPrimaRSP = devolucionPrimaRSP;
		this.textsResponse = textsResponse;
		this.message = message;
	}

	
}
