package cl.cardif.DevolucionPrima.domain.Response;

public class DevPrimaFunctionRSP {

		private String permiteDevolver;
		private String formula;
		private String fecvalorUfCal;
		private String estadoProcesDev;
		
		
		public DevPrimaFunctionRSP(String permiteDevolver, String formula, String fecvalorUfCal,
				String estadoProcesDev) {
			super();
			this.permiteDevolver = permiteDevolver;
			this.formula = formula;
			this.fecvalorUfCal = fecvalorUfCal;
			this.estadoProcesDev = estadoProcesDev;
		}
		public String getPermiteDevolver() {
			return permiteDevolver;
		}
		public String getFormula() {
			return formula;
		}
		public String getFecvalorUfCal() {
			return fecvalorUfCal;
		}
		public String getEstadoProcesDev() {
			return estadoProcesDev;
		}
		
		

		
		
}
