package cl.cardif.DevolucionPrima.domain.Response;

import java.sql.Date;

public class CoreRSP {

	private String estadoOperacion;
	private Date fechaInicioSeguro;
	private Date fechaFinSeguro;
	private String operacionCardif;
	private String polizaMadre;
	private String permiteDevolver;
	private String montoDevolucionUF;
	private String montoDevolucionPesos;
	private String formula;
	private String fechaValorUfCal;
	private String primaBruta;
	private String tipoMoneda;
	private String estadoProcesDev;
	public CoreRSP(String estadoOperacion, Date fechaInicioSeguro, Date fechaFinSeguro, String operacionCardif,
			String polizaMadre, String permiteDevolver, String montoDevolucionUF, String montoDevolucionPesos,
			String formula, String fechaValorUfCal, String primaBruta, String tipoMoneda,
			String estadoProcesDev) {
		super();
		this.estadoOperacion = estadoOperacion;
		this.fechaInicioSeguro = fechaInicioSeguro;
		this.fechaFinSeguro = fechaFinSeguro;
		this.operacionCardif = operacionCardif;
		this.polizaMadre = polizaMadre;
		this.permiteDevolver = permiteDevolver;
		this.montoDevolucionUF = montoDevolucionUF;
		this.montoDevolucionPesos = montoDevolucionPesos;
		this.formula = formula;
		this.fechaValorUfCal = fechaValorUfCal;
		this.primaBruta = primaBruta;
		this.tipoMoneda = tipoMoneda;
		this.estadoProcesDev = estadoProcesDev;
	}
	public String getEstadoOperacion() {
		return estadoOperacion;
	}
	public Date getFechaInicioSeguro() {
		return fechaInicioSeguro;
	}
	public Date getFechaFinSeguro() {
		return fechaFinSeguro;
	}
	public String getOperacionCardif() {
		return operacionCardif;
	}
	public String getPolizaMadre() {
		return polizaMadre;
	}
	public String getPermiteDevolver() {
		return permiteDevolver;
	}
	public String getMontoDevolucionUF() {
		return montoDevolucionUF;
	}
	public String getMontoDevolucionPesos() {
		return montoDevolucionPesos;
	}
	public String getFormula() {
		return formula;
	}
	public String getFechaValorUfCal() {
		return fechaValorUfCal;
	}
	public String getPrimaBruta() {
		return primaBruta;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public String getEstadoProcesDev() {
		return estadoProcesDev;
	}
	

}
	
