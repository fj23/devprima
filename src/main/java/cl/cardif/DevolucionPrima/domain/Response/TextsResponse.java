package cl.cardif.DevolucionPrima.domain.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TextsResponse {
	@JsonProperty("Documentos")
	Documentos documentos;
	@JsonProperty("TextoPorDefecto")
	String textoXDefecto;
	public TextsResponse(Documentos documentos, String textoXDefecto) {
		super();
		this.documentos = documentos;
		this.textoXDefecto = textoXDefecto;
	}

}
