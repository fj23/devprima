package cl.cardif.DevolucionPrima.domain.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Documentos {
	@JsonProperty("NombreDocumento")
	String nombreDocumento;
	@JsonProperty("Código")
	String codigo;
	public Documentos(String nombreDocumento, String codigo) {
		super();
		this.nombreDocumento = nombreDocumento;
		this.codigo = codigo;
	}
}
