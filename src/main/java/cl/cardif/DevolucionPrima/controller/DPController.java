package cl.cardif.DevolucionPrima.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.DevolucionPrima.domain.Request.REQDP;
import cl.cardif.DevolucionPrima.domain.Response.ResponseDP;
import cl.cardif.DevolucionPrima.service.DPService;

@RestController
@RequestMapping("/")
public class DPController {

	@Autowired
	DPService dpService;
	@PostMapping(
			value = "devolucion-prima"
			)
	public ResponseEntity<ResponseDP>validate(@RequestBody REQDP reqdp) throws SQLException{
		
		return new ResponseEntity<>(dpService.validate(reqdp), HttpStatus.OK);
		
	}
	
}
