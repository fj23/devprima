package cl.cardif.DevolucionPrima.tools;

public class DPConstants{

public static final String JAVA_COMP = "java:comp/env";
public static final String JNDI_EASI = "jdbc/EASI";
public static final String JNDI_ERAN = "jdbc/ERAN";
public static final String JNDI_DEV = "jdbc/Consulta_Devp";
public static final String JNDI_devprima = "jdbc/devprima";

public static final String FORMAT_DATE = "dd/MM/yyyy";

public static final String PRC_CONS_VALORES_DEVMATRIZ =
	"{call PCK_CONSULTA_DEVPRIMA.Prc_Cons_Valores_DevMatriz(?,?,?,?,?,?)}";

public static final String PRC_CONSULTATEXTOANEXO =
"{call PCK_CONSULTA_DEVPRIMA.Prc_Consultatextoanexo(?,?,?)}";

public static final String PRC_MOTIVO_CANCELACION =
    "{call PCK_CONSULTA_DEVPRIMA.Prc_BusqMotCancel(?,?)}";

public static final String PRC_CONSULTAPOLPRIMABRUTA = 
	"{call PCK_CONSULTA_DEVPRIMA.Prc_Cons_PolPrimaUnica(?,?,?,?,?,?,?)}";

public static final String PRC_CONSULTAPOLPRIMABRUTAEASI = 
"{call PCK_CONSULTA_DEVPRIMA.Prc_Cons_PolPrimaUnica(?,?,?,?,?)}";

public static final String PRC_CONSULTARETORNOSERVICIO = 
"{call PCK_CONSULTA_DEVPRIMA.Prc_ConsultaRetornaServicio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

public static final String REGEX_DATE =
    "^(0?[1-9]|[12][0-9]|3[01])[\\/\\-](0?[1-9]|1[012])[\\/\\-]\\d{4}$";
public static final String REGEX_DNI = "^[0-9Kk]+$";

public static final int NUMBER_ZERO = 0;
public static final int NUMBER_ONE = 1;
public static final int NUMBER_TWO = 2;
public static final int NUMBER_EIGHT = 8;
public static final int NUMBER_TEN = 10;
public static final int NUMBER_ELEVEN = 11;
public static final String DVK = "K";
public static final String SLASH = "/";
public static final String FLAG = "R";
public static final String EMPTY = "";
}