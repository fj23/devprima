package cl.cardif.DevolucionPrima.tools;

public class DPMessages {

	  public static final String OK = "Servicio se ejecuto correctamente";
	  public static final String ERROR_RUT = "Rut no válido";
	  public static final String ERROR_VALIDATION_DNI_OR_DATE = "Error en la validacion del rut asegurado o en la fecha vigencia";
	  public static final String ERROR_MANDATORY = "Faltan datos obligatorios en la búsqueda";
	  public static final String ERROR_DATE = "Error en la fecha de vigencia";
	  public static final String ERROR_STORE_PROCEDURE = "Error en el Procedimiento Almacenado : ";
	  public static final String ERROR_MOTIVO = "Motivo devolución no válido";
	}
