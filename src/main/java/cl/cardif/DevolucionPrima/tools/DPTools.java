package cl.cardif.DevolucionPrima.tools;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Service;

import cl.cardif.DevolucionPrima.domain.Request.REQDP;

@Service
public class DPTools {
	
	 public static String dateToString(Date date) {
		    if (date != null) {
		      DateFormat df = new SimpleDateFormat(DPConstants.FORMAT_DATE);
		      return df.format(date);
		    }
		    return DPConstants.EMPTY;
		  }

	 public static Date stringToDate(String fecha) throws ParseException {
		 		SimpleDateFormat format = new SimpleDateFormat(DPConstants.FORMAT_DATE);
		 		java.util.Date parsed= format.parse(fecha);
		 		Date sqlDate = new Date(parsed.getTime());
		 		return sqlDate;
		  }
	 
	public static boolean validarRut(String rut) {

	    boolean validacion = false;
	    try {
	        rut =  rut.toUpperCase();
	        rut = rut.replace(".", "");
	        rut = rut.replace("-", "");
	        int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

	        char dv = rut.charAt(rut.length() - 1);

	        int m = 0, s = 1;
	        for (; rutAux != 0; rutAux /= 10) {
	            s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
	        }
	        if (dv == (char) (s != 0 ? s + 47 : 75)) {
	            validacion = true;
	        }

	    } catch (java.lang.NumberFormatException e) {
	    } catch (Exception e) {
	    }
	    return validacion;
	}
	
	public boolean mandatoryInput(REQDP requestDp) {
		return requestDp.getDevolucionPrimaREQ().getIdSocio()!=null
				&& !requestDp.getDevolucionPrimaREQ().getIdSocio().isEmpty()
				&& requestDp.getDevolucionPrimaREQ().getRutAsegurado()!=null
				&& !requestDp.getDevolucionPrimaREQ().getRutAsegurado().isEmpty()
				&& requestDp.getDevolucionPrimaREQ().getOperacionSocio()!=null
				&& !requestDp.getDevolucionPrimaREQ().getOperacionSocio().isEmpty()
				&& requestDp.getDevolucionPrimaREQ().getPolizaIndividual()!=null
				&& !requestDp.getDevolucionPrimaREQ().getPolizaIndividual().isEmpty()
				&& requestDp.getDevolucionPrimaREQ().getMotivoCancelacion()!=null
				&& !requestDp.getDevolucionPrimaREQ().getMotivoCancelacion().isEmpty()
				&& !requestDp.getDevolucionPrimaREQ().getFechaTermino().isEmpty()
				&&  requestDp.getDevolucionPrimaREQ().getFechaTermino()!= null;
		
	}
}
