package cl.cardif.DevolucionPrima.dao.Impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import cl.cardif.DevolucionPrima.dao.DPDao;
import cl.cardif.DevolucionPrima.domain.Request.DevolucionPrimaREQ;
import cl.cardif.DevolucionPrima.domain.Request.REQDP;
import cl.cardif.DevolucionPrima.domain.Response.CoreRSP;
import cl.cardif.DevolucionPrima.domain.Response.DevPrimaFunctionRSP;
import cl.cardif.DevolucionPrima.domain.Response.DevolucionPrimaRSP;
import cl.cardif.DevolucionPrima.domain.Response.Documentos;
import cl.cardif.DevolucionPrima.domain.Response.MotivosRsp;
import cl.cardif.DevolucionPrima.domain.Response.TextsResponse;
import cl.cardif.DevolucionPrima.exception.DPException;
import cl.cardif.DevolucionPrima.tools.DPConstants;
import cl.cardif.DevolucionPrima.tools.DPTools;
import oracle.jdbc.OracleTypes;

@Repository
public class DPDaoImpl implements DPDao  {
	@Autowired
	 DPTools dpTools;
	
	@Override
	public List<DevolucionPrimaRSP> getfinalRSP(DevolucionPrimaREQ req, List<CoreRSP> coreRspList, MotivosRsp motivo) throws SQLException {
		 List<DevolucionPrimaRSP> DPlist = new ArrayList<DevolucionPrimaRSP>();
		 DevolucionPrimaRSP devolucionPrimaRsp;
		 List<CoreRSP> cRspList = coreRspList;
		 CoreRSP crsp;
		 Date fechaTermino = null;
		try {
			fechaTermino = DPTools.stringToDate(req.getFechaTermino());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			System.out.println(e1);
		}
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(DPConstants.JAVA_COMP))
				.lookup(DPConstants.JNDI_DEV)).getConnection();
				CallableStatement call = conn.prepareCall(DPConstants.PRC_CONSULTARETORNOSERVICIO))
				{
				conn.setAutoCommit(false);
				int codTrans = 1;
				for (int i = 0; i < cRspList.size(); i++) {
					crsp = cRspList.get(i);
					call.setInt(1, codTrans);
					codTrans++;
					call.setString(2, req.getIdSocio());
					call.setString(3, req.getRutAsegurado());
					call.setString(4, req.getOperacionSocio());
					call.setString(5, req.getPolizaIndividual());
					call.setString(6, motivo.getCodigoMotivo());
					call.setString(7, crsp.getPolizaMadre());
					call.setDate(8,  fechaTermino);
					call.setString(9, req.getNombreEjecutivo());
					call.setString(10, req.getSucursal());
					call.setString(11, req.getNombreCargo());
					call.setString(12, req.getIdefJefeEjecutivo());
					call.setString(13, crsp.getEstadoOperacion());
					call.setDate(14,  crsp.getFechaInicioSeguro());
					call.setDate(15,  crsp.getFechaFinSeguro());
					call.setString(16, crsp.getOperacionCardif());					
					call.setString(17, crsp.getPermiteDevolver());
					call.setString(18, crsp.getMontoDevolucionUF());
					call.setString(19, crsp.getMontoDevolucionPesos());
					call.setString(20, crsp.getFormula());
					call.setString(21, crsp.getFechaValorUfCal());
					call.setString(22, crsp.getPrimaBruta());
					call.setString(23, crsp.getTipoMoneda());
					call.setString(24, crsp.getEstadoProcesDev());
					call.setInt(25, 0);
					call.setInt(26, 0);
					call.registerOutParameter(27, OracleTypes.VARCHAR);
				    call.registerOutParameter(28, OracleTypes.CURSOR);
				    call.execute();
				    if(call.getString(27).equals("OK")) {
				    	ResultSet rsDP = (ResultSet) call.getObject(28);
				    	while(rsDP.next()) {
							 devolucionPrimaRsp = new DevolucionPrimaRSP(
									 rsDP.getString("OPECARDIF"),
									 rsDP.getString("CODPOLMADRE"),
									 crsp.getPermiteDevolver(),
									 rsDP.getString("MONTODEVUF"), 
									 rsDP.getString("MONTODEVPESOS"),
									 DPTools.dateToString(rsDP.getDate("FECUFUTILIZADA")),
									 rsDP.getString("VALUFUTILIZADA"),
									 rsDP.getString("GLSERRORDESCRIPCION"));
							 DPlist.add(devolucionPrimaRsp);
						 }
						 
				    }else {
				    	throw new DPException(call.getString(27), HttpStatus.INTERNAL_SERVER_ERROR, null);
				    }
				}
				return DPlist;
		 
				}catch(SQLException | NamingException e) {
					throw new DPException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
				}
	}

	@Override
	public List<CoreRSP> getPrimaEran(REQDP requestDP, MotivosRsp motivos) throws DPException {
		List<CoreRSP> DPlist = new ArrayList<CoreRSP>();
		DevolucionPrimaREQ req= requestDP.getDevolucionPrimaREQ();
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(DPConstants.JAVA_COMP))
				.lookup(DPConstants.JNDI_ERAN)).getConnection();
				CallableStatement call = conn.prepareCall(DPConstants.PRC_CONSULTAPOLPRIMABRUTA))
				{
				conn.setAutoCommit(false);
				call.setString(1, req.getIdSocio());
				call.setString(2, req.getRutAsegurado());
				call.setString(3, req.getOperacionSocio());
				call.setString(4, motivos.getCodigoMotivo());
				call.setString(5, req.getPolizaMadre());
				call.registerOutParameter(6, OracleTypes.VARCHAR);
			    call.registerOutParameter(7, OracleTypes.CURSOR);
				call.execute();
				if(call.getString(6).equals("OK")) {
					ResultSet rsDP = (ResultSet) call.getObject(7);
					 List<CoreRSP> coreRspList = new ArrayList<CoreRSP>();
					 CoreRSP coreRsp;
					 while(rsDP.next()) {
						 coreRsp = new CoreRSP(
								 rsDP.getString("ESTADO_OPER"),
								 rsDP.getDate("FECINI_SEGURO"),
								 rsDP.getDate("FECFIN_SEGURO"),
								 rsDP.getString("OPESOCIO_CARDIF"),
								 rsDP.getString("CODPOL_MADRE"),
								 rsDP.getString("PERMITE_DEV"),
								 rsDP.getString("MONTO_DEVUF"),
								 rsDP.getString("MONTO_DEVPESOS"),
								 rsDP.getString("FORMULA"),
								 rsDP.getString("FECVALOR_UFCALC"),
								 rsDP.getString("PRIMA_BRUTA"),
								 rsDP.getString("TIPO_MONEDA"),
								 rsDP.getString("ESTADO_PROCESDEV"));
						 coreRspList.add(coreRsp);
					 }
					 DPlist = coreRspList;
				}else {
					return DPlist = null;
			      }
			      conn.commit();
			      conn.setAutoCommit(true);
						
		}catch(SQLException | NamingException e) {
			throw new DPException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		return DPlist;
	}
	

	@Override
	public List<CoreRSP> getPrimaEasi(REQDP requestDP, MotivosRsp motivos) throws DPException {		
		List<CoreRSP> DPlist = new ArrayList<CoreRSP>();
		CoreRSP crsp;
		DevolucionPrimaREQ req= requestDP.getDevolucionPrimaREQ();	
		DevPrimaFunctionRSP devPfuncRsp = null;
		String permiteDev = "";
		String formula = "";
		String fechaValorUfCalc = "";
		String stadoProcessDev = "";
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(DPConstants.JAVA_COMP))
				.lookup(DPConstants.JNDI_EASI)).getConnection();
				CallableStatement call = conn.prepareCall(DPConstants.PRC_CONSULTAPOLPRIMABRUTAEASI))
				{
			conn.setAutoCommit(false);
			call.setString(1, req.getRutAsegurado());
			call.setString(2, req.getOperacionSocio());
			call.setString(3, req.getPolizaMadre());
			call.registerOutParameter(4, OracleTypes.VARCHAR);
		    call.registerOutParameter(5, OracleTypes.CURSOR);
		    call.execute();	
		    if(call.getString(4).equals("OK")) {
		    	ResultSet rsDP = (ResultSet) call.getObject(5);
		    	while(rsDP.next()) {
		    		if(rsDP.getString("ESTADO_OPER").equals("NO VIGENTE")){
		    			permiteDev = "NO";
		    			formula = "";
		    			fechaValorUfCalc = "";
		    			stadoProcessDev = "N/A";
		    		}else {
		    			devPfuncRsp = getDevPrimaFunctionRSP(requestDP, rsDP, motivos);
		    			if(devPfuncRsp!=null){
		    				permiteDev = devPfuncRsp.getPermiteDevolver();
			    			formula = devPfuncRsp.getFormula();
			    			fechaValorUfCalc = devPfuncRsp.getFecvalorUfCal();
			    			stadoProcessDev = devPfuncRsp.getEstadoProcesDev();
		    			}else {
		    				permiteDev = "NO";
			    			formula = "";
			    			fechaValorUfCalc = "";
			    			stadoProcessDev = "N/A";
		    			}
		    		}		    			    		
		    	crsp = new CoreRSP(
		    			 rsDP.getString("ESTADO_OPER"),
						 rsDP.getDate("FECINI_SEGURO"),
						 rsDP.getDate("FECFIN_SEGURO"),
						 rsDP.getString("OPESOCIO_CARDIF"),
						 rsDP.getString("CODPOL_MADRE"),
						 permiteDev,
						 rsDP.getString("MONTO_DEVUF"),
						 rsDP.getString("MONTO_DEVPESOS"),
						 formula,
						 fechaValorUfCalc,
						 rsDP.getString("PRIMA_BRUTA"),
						 rsDP.getString("TIPO_MONEDA"),
						 stadoProcessDev);
		    	DPlist.add(crsp);
		    	}
		    }else {
		    	throw new DPException(call.getString(4), HttpStatus.INTERNAL_SERVER_ERROR, null);
		    }
		    conn.commit();
		    conn.setAutoCommit(true);
		}catch(SQLException | NamingException e) {
			throw new DPException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		return DPlist;
	}
	
	private DevPrimaFunctionRSP getDevPrimaFunctionRSP(REQDP requestDP, ResultSet rsDP, MotivosRsp motivos) {
		DevPrimaFunctionRSP devPRsp = null;
		DevolucionPrimaREQ req= requestDP.getDevolucionPrimaREQ();
		String opeSocioCardif="";
		String polizaMadre="";
		try {
			opeSocioCardif = rsDP.getString("OPESOCIO_CARDIF");
			polizaMadre = rsDP.getString("CODPOL_MADRE");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.out.println("e"+ e1);
		}	
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(DPConstants.JAVA_COMP))
				.lookup(DPConstants.JNDI_devprima)).getConnection();
				CallableStatement call = conn.prepareCall(DPConstants.PRC_CONS_VALORES_DEVMATRIZ))
				{
						conn.setAutoCommit(false);
						call.setString(1, req.getRutAsegurado());
						call.setString(2, opeSocioCardif);
						call.setString(3, polizaMadre);
						call.setString(4, motivos.getCodigoMotivo());
						call.registerOutParameter(5, OracleTypes.VARCHAR);
						call.registerOutParameter(6, OracleTypes.CURSOR);					
						call.execute();
						if(call.getString(5).equals("OK")) {					
							ResultSet matrizRsp = (ResultSet) call.getObject(6);
							while(matrizRsp.next()) {
							devPRsp = new DevPrimaFunctionRSP(matrizRsp.getString("PERMITE_DEV"),
									matrizRsp.getString("FORMULA"),
									matrizRsp.getString("FECVALOR_UFCALC"),
									matrizRsp.getString("ESTADO_PROCESDEV"));
							}
						}else {
							return devPRsp= null;
					    }
					    conn.commit();
					    conn.setAutoCommit(true);
						
					    return devPRsp;				
					
				}catch(SQLException | NamingException e) {
					throw new DPException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
				}
	}
	

	@Override
	public List<MotivosRsp> getMotivos(REQDP requestDP) {
		List<MotivosRsp> motivosList = new ArrayList<MotivosRsp>();
		MotivosRsp motivos = null;
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(DPConstants.JAVA_COMP))
				.lookup(DPConstants.JNDI_DEV)).getConnection();
				CallableStatement call = conn.prepareCall(DPConstants.PRC_MOTIVO_CANCELACION))
				{
				conn.setAutoCommit(false);
				call.registerOutParameter(1, OracleTypes.VARCHAR);
				call.registerOutParameter(2, OracleTypes.CURSOR);
				call.execute();
				if(call.getString(1).equals("OK")) {
					ResultSet m = (ResultSet) call.getObject(2);
					while(m.next()) {
						motivos = new MotivosRsp(m.getString("CODMOTIVO"),
								m.getString( "DESCRIPCION"));
						motivosList.add(motivos);
					}						
					m.close();
					return motivosList;
				}else {
			    	throw new DPException(call.getString(1), HttpStatus.INTERNAL_SERVER_ERROR, null);
			    }
									
		}catch(SQLException | NamingException e) {
			throw new DPException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
	}
	
	@Override
	public List<TextsResponse> getTexts(MotivosRsp motivos){
		List<TextsResponse> textList = new ArrayList<TextsResponse>();
		TextsResponse textResponse = null;
		Documentos documentos = null;
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(DPConstants.JAVA_COMP))
				.lookup(DPConstants.JNDI_DEV)).getConnection();
				CallableStatement call = conn.prepareCall(DPConstants.PRC_CONSULTATEXTOANEXO))
				{
					conn.setAutoCommit(false);
					call.setString(1, motivos.getCodigoMotivo());
					call.registerOutParameter(2, OracleTypes.VARCHAR);
					call.registerOutParameter(3, OracleTypes.CURSOR);
					call.execute();
					if(call.getString(2).equals("OK")){
						ResultSet textRs = (ResultSet) call.getObject(3);
						while(textRs.next()){
							documentos = new Documentos(textRs.getString("DESCRIPCION"),
									textRs.getString("CODTEXTO"));
							textResponse = new TextsResponse(documentos,
									textRs.getString("DOOCUMENTOS"));
							textList.add(textResponse);
						}
						return textList;
					}else {
						return textList = null;
					}
				}catch(SQLException | NamingException e) {
					throw new DPException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
				}		
	}
	
}
