package cl.cardif.DevolucionPrima.dao;

import java.sql.SQLException;
import java.util.List;

import cl.cardif.DevolucionPrima.domain.Request.DevolucionPrimaREQ;
import cl.cardif.DevolucionPrima.domain.Request.REQDP;
import cl.cardif.DevolucionPrima.domain.Response.CoreRSP;
import cl.cardif.DevolucionPrima.domain.Response.DevolucionPrimaRSP;
import cl.cardif.DevolucionPrima.domain.Response.MotivosRsp;
import cl.cardif.DevolucionPrima.domain.Response.TextsResponse;
import cl.cardif.DevolucionPrima.exception.DPException;

public interface  DPDao {
	public List<CoreRSP> getPrimaEran(REQDP requestDP, MotivosRsp motivosRsp) throws DPException;
	public List<CoreRSP> getPrimaEasi(REQDP requestDP, MotivosRsp motivosRsp) throws DPException;
	public List<MotivosRsp> getMotivos(REQDP requestDP);
	public List<DevolucionPrimaRSP> getfinalRSP(DevolucionPrimaREQ req,List<CoreRSP> coreRsp, MotivosRsp motivosRsp) throws SQLException;
	public List<TextsResponse> getTexts(MotivosRsp motivosRsp);
}
