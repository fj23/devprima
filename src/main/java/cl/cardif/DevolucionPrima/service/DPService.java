package cl.cardif.DevolucionPrima.service;

import java.sql.SQLException;

import cl.cardif.DevolucionPrima.domain.Request.REQDP;
import cl.cardif.DevolucionPrima.domain.Response.ResponseDP;

public interface DPService {
	ResponseDP validate(REQDP requestDP) throws SQLException;
}
