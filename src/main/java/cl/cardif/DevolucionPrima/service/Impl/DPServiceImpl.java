package cl.cardif.DevolucionPrima.service.Impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.DevolucionPrima.dao.DPDao;
import cl.cardif.DevolucionPrima.domain.Request.DevolucionPrimaREQ;
import cl.cardif.DevolucionPrima.domain.Request.REQDP;
import cl.cardif.DevolucionPrima.domain.Response.CoreRSP;
import cl.cardif.DevolucionPrima.domain.Response.DevolucionPrimaRSP;
import cl.cardif.DevolucionPrima.domain.Response.Message;
import cl.cardif.DevolucionPrima.domain.Response.MotivosRsp;
import cl.cardif.DevolucionPrima.domain.Response.ResponseDP;
import cl.cardif.DevolucionPrima.domain.Response.TextsResponse;
import cl.cardif.DevolucionPrima.exception.DPException;
import cl.cardif.DevolucionPrima.service.DPService;
import cl.cardif.DevolucionPrima.tools.DPMessages;
import cl.cardif.DevolucionPrima.tools.DPTools;

@Service
public class DPServiceImpl implements DPService {
	
	@Autowired
	DPTools dpTools;
	@Autowired
	DPDao dpDao;
	
	@Override
	public ResponseDP validate(REQDP requestDP) throws SQLException {
		List<DevolucionPrimaRSP> dPlist = new ArrayList<DevolucionPrimaRSP>();
		List<CoreRSP> crRspEran = new ArrayList<CoreRSP>();
		List<CoreRSP> crRspEasi = new ArrayList<CoreRSP>();
		DevolucionPrimaREQ dPRqs = requestDP.getDevolucionPrimaREQ();
		List<TextsResponse> textResponse = new ArrayList<TextsResponse>();
		DevolucionPrimaREQ req = requestDP.getDevolucionPrimaREQ();
		String rutAsegurado = req.getRutAsegurado();
		String operacionSocio = req.getOperacionSocio();
		String polizaIndividual = req.getPolizaIndividual();
		String fechaTerminoAnticipado = req.getFechaTermino();
		String motivoDevolucion = req.getMotivoCancelacion();
		try {
		if(dpTools.mandatoryInput(requestDP)){
			if(DPTools.validarRut(requestDP.getDevolucionPrimaREQ().getRutAsegurado())) {
				List<MotivosRsp> motivos = dpDao.getMotivos(requestDP);
				MotivosRsp motivo = null;
				String motiv="";
				String codigoMotivo="";
				for (int i = 0; i < motivos.size(); i++) {
					if(motivos.get(i).getMotivo().equals(requestDP.getDevolucionPrimaREQ().getMotivoCancelacion())){
						motiv = motivos.get(i).getMotivo();
						codigoMotivo = motivos.get(i).getCodigoMotivo();
						motivo = new MotivosRsp(codigoMotivo, motiv );
					}
				}
				if(motivo!=null) {
					textResponse = dpDao.getTexts(motivo);
					crRspEran= dpDao.getPrimaEran(requestDP, motivo);
						if(crRspEran!=null) {
							dPlist= dpDao.getfinalRSP(dPRqs, crRspEran,motivo);
							return new ResponseDP(rutAsegurado,
									operacionSocio,
									polizaIndividual,
									fechaTerminoAnticipado,
									motivoDevolucion,
									dPlist,
									textResponse,
									new Message(DPMessages.OK,10));
						}else {
							crRspEasi = dpDao.getPrimaEasi(requestDP, motivo);
							if(crRspEasi.size()>0) {
								dPlist= dpDao.getfinalRSP(dPRqs, crRspEasi, motivo);
								return new ResponseDP(rutAsegurado,
										operacionSocio,
										polizaIndividual,
										fechaTerminoAnticipado,
										motivoDevolucion,
										dPlist,
										textResponse,
										new Message(DPMessages.OK,10));
							}else {
								return new ResponseDP(null,null,null,null,null,null,null, new Message(DPMessages.ERROR_STORE_PROCEDURE,13));
							}
						}
									
				}else {
					return new ResponseDP(null,null,null,null,null,null,null, new Message(DPMessages.ERROR_MOTIVO, 3));
				}
				
			}else {
				return new ResponseDP(null,null,null,null,null,null,null, new Message(DPMessages.ERROR_RUT, 2));
			}
			
		}else {
			return new ResponseDP(null,null,null,null,null,null,null, new Message(DPMessages.ERROR_MANDATORY, 1));
		}
	}catch(DPException e){
		return new ResponseDP(null,null,null,null,null,null,null, new Message(DPMessages.ERROR_STORE_PROCEDURE + e.getMessage(), 13));
	}
}
	}
